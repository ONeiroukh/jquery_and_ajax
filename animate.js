<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script> 
$(document).ready(function(){
  $("#bu1").click(function(){
    var div = $("div");  
    div.animate({left: '100px'}, "slow");
    div.animate({fontSize: '5em',paddingTop: '1em',paddingLeft: '0.3em',width:'300px'}, "slow");
    div.animate({color:"red"}, "slow");
  });
 $("#bu2").click(function(){
    var div = $("div"); 
   div.animate({left:"0"}, "slow");
    div.animate({background:'#98bf21'}, "slow");  div.animate({fontSize: '1em',paddingTop: '.2em',height:'100px',paddingLeft: '0.3em',width:'200px'}, "slow");
    
    div.animate({color:"black"}, "slow");
  });});
</script> 
</head>
<body>

<button id="bu1">Start Animation</button>
<button id="bu2">Start agin Animation</button>
<p>By default, all HTML elements have a static position, and cannot be moved. To manipulate the position, remember to first set the CSS position property of the element to relative, fixed, or absolute!</p>

<div style="background:#98bf21;height:100px;width:200px;position:absolute;">HELLO</div>

</body>
</html>
